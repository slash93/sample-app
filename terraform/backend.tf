terraform {

    required_version = ">= 0.13"

    backend "http" {
    }

    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
            version = "3.6.0"
        }

        kubectl = {
            source  = "gavinbunney/kubectl"
            version = ">= 1.7.0"
        }

        helm = {
            source = "hashicorp/helm"
            version = "2.2.0"
        }
    }
}