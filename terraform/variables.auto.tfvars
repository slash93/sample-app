gcp_region = "us-central1"
gke_cluster_name = "gke-sample-cluster"
gke_zones = ["us-central1-c"]
gke_network = "default"
gke_subnetwork = "default"
gke_node_pool_name = "sample-pool"