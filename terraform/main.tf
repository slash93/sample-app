
module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = var.gcp_project_id
  name                       = var.gke_cluster_name
  region                     = var.gcp_region
  regional                   = false
  zones                      = var.gke_zones
  network                    = var.gke_network
  subnetwork                 = var.gke_subnetwork
  ip_range_pods              = ""
  ip_range_services          = ""
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  network_policy             = false
  remove_default_node_pool   = true

  node_pools = [
    {
      name                      = var.gke_node_pool_name
      machine_type              = "e2-medium"
      min_count                 = 1
      max_count                 = 3
      local_ssd_count           = 0
      disk_size_gb              = 100
      disk_type                 = "pd-standard"
      image_type                = "cos_containerd"
      auto_repair               = true
      auto_upgrade              = true
      service_account           = var.gke_service_account_name
      preemptible               = false
      initial_node_count        = 3
    },
  ]

}

resource "kubectl_manifest" "app" {
    yaml_body = file("deployment/app.yml")
}

resource "kubectl_manifest" "service" {
    yaml_body = file("deployment/service.yml")
    depends_on = [kubectl_manifest.app]
}


resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  depends_on = [
    kubernetes_namespace.cert_manager,
  ]
  name       = "cert-manager"
  namespace  = kubernetes_namespace.cert_manager.metadata[0].name
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  set {
    name  = "installCRDs" # <- note this
    value = "true"
  }
}


resource "kubectl_manifest" "clusterissuer" {
    yaml_body = file("deployment/clusterissuer.yml")
    depends_on = [helm_release.cert_manager]
}

resource "kubernetes_ingress" "clusterissuer" {
  depends_on = [kubectl_manifest.clusterissuer]
  wait_for_load_balancer = true
  metadata {
    name = "sample-app-ingress"
    labels = {
      app = "sample-app"
    }

    annotations = {
      "acme.cert-manager.io/http01-edit-in-place" = "true"
      "cert-manager.io/cluster-issuer" = "letsencrypt-prod"
      "kubernetes.io/ingress.global-static-ip-name" = "web-static-ip"
    }
  }

  spec {
    tls {
      hosts       = ["zapotal.club"]
      secret_name = "sample-app-cert-secret"
    }

    rule {
      host = "zapotal.club"
      http {
        path {
          path = "/*"
          backend {
            service_name = "sample-app-service"
            service_port = "80"
          }
        }
      }
    }

  }
}

