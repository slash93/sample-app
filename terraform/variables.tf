variable "gcp_credentials" {
  type = string
  description = "Location of service account for GCP"
}

variable "gcp_project_id" {
  type = string
  description = "GCP Project id"
}

variable "gcp_region" {
  type = string
  description = "GCP Region"
}

variable "gke_cluster_name" {
  type = string
  description = "GCP Cluster name"
}

variable "gke_zones" {
  type = list(string)
  description = "List of Zones for GKE cluster"
}

variable "gke_network" {
  type = string
  description = "VPC Network name"
}

variable "gke_subnetwork" {
  type = string
  description = "VPC Sub Network name"
}

variable "gke_node_pool_name" {
  type = string
  description = "GKE node pool name"
}

variable "gke_service_account_name" {
  type = string
  description = "GKE service account name"
}

variable "gitlab_access_token" {
  type = string
  description = "GitLab Token"
}

